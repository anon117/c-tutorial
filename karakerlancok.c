#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

char ** mystrings(char ** arr1, char ** arr2, size_t a1size, size_t a2size) {
    char **ptr = (char**) malloc((a1size + a2size) / sizeof(char *));
    memcpy(ptr, arr1, a1size);
    memcpy(ptr + a1size/sizeof(char *), arr2, a2size);
    return ptr;
}

char * myconcate(char * ch1, char * ch2) {
    char * result = (char *) malloc(1 + strlen(ch1) + strlen(ch2));
    strcpy(result, ch1);
    strcat(result, ch2);
    return result;
}

char * tomb1[] = {"alma", "körte", "szilva", "dinnye"};
char * tomb2[] = {"narancs", "paradicsom", "mandarin", "eper", "cseresznye", "dió"};

char * elso = "első karakterlánc - ";
char * masodik = "masodik karakterlánc";

int main(){
    printf("%s\n", myconcate(elso, masodik));

    int darab = (sizeof(tomb1) + sizeof(tomb2)) / sizeof(char *) ;
    char ** pp = mystrings(tomb1, tomb2, sizeof(tomb1), sizeof(tomb2));
    for (int i = 0; i < darab; i++) {
        printf("%s\n", pp[i] );
    }
    return 0;
}
