#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

template <class TYPE>
class TVector {
  public:
    TYPE x, y;
    TVector(){}
    TVector(TYPE a, TYPE b) : x(a), y(b) {}
};

template <class TYPE>
TVector<TYPE> operator+ (const TVector<TYPE> bal, const TVector<TYPE> jobb) {
  TVector<TYPE> kimenet;
  kimenet.x = bal.x + jobb.x;
  kimenet.y = bal.y + jobb.y;
  return kimenet;
}

int main() {
  TVector<int> a1(3, 4);
  TVector<int> a2(7, 9);
  TVector<double> b1(3.7, 5.6);
  TVector<double> b2(5.6, 7.5);

  TVector<int> result1;
  TVector<double> result2;

  result1 = a1 + a2;
  result2 = b1 + b2;

  cout << "(" << result1.x << "," << result1.y << ")" << endl;
  cout << "(" << result2.x << "," << result2.y << ")" << endl;

  return 0;
}
