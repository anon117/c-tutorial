#include <iostream>
#include <cstdlib>

using namespace std;

// prototípusok
int osszead(int i, int j);
double osszead(double a, double b, double c);

// függvény overloading
int osszead(int i, int j) {
  return i + j;
}

double osszead(double a, double b, double c) {
  return a + b + c;
}

int main(){
  cout << osszead(3, 4) << endl; // 7
  cout << osszead(1.1, 2.9, 3.7) << endl; // 7.7

  return 0;
}
