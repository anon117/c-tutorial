#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;


class Osztaly {
  private:
    int i;
    //std::string str ("Test string");
    string str1;

  public:
    Osztaly();
    Osztaly(int j, string s);
    Osztaly(const Osztaly& o);
    int getInt();
    string getString();
};

Osztaly::Osztaly() {
}

Osztaly::Osztaly(int j, string s) {
  i = j; str1 = s;
}

Osztaly::Osztaly(const Osztaly& v) {
  i = v.i; str1 = v.str1;
}

int Osztaly::getInt() {
  return i;
}

string Osztaly::getString() {
  return str1;
}

int main() {
  Osztaly a(5, "apple");
  Osztaly b(a);

  cout << b.getInt() << " " << b.getString() << endl;

  return 0;
}
