#include <cstdlib>
#include <iostream>

using namespace std;

class Osztaly {
    private:
        int x,y;
    public:
        Osztaly(){}
        void setX(int inX);
        int getX() const ;
        void setY(int inY);
        int getY() const;

        friend Osztaly operator+ (const Osztaly& a, const Osztaly& b);
        friend Osztaly operator- (const Osztaly& a, const Osztaly& b);
        Osztaly operator+= (const Osztaly& a);
        Osztaly operator++ (int); // a++ (postfix)
        // Osztaly& operator++ (); // ++a (prefix)
};

void Osztaly::setX(int inX) {
    x = inX;
}

int Osztaly::getX() const {
    return x;
}

void Osztaly::setY(int inY) {
    y = inY;
}

int Osztaly::getY() const {
    return y;
}

Osztaly operator+(const Osztaly& a, const Osztaly& b) {
    Osztaly output;
    output.setX(a.getX() + b.getX());
    output.setY(a.getY() + b.getY());

    return output;
}

Osztaly operator- (const Osztaly& a, const Osztaly& b) {
    Osztaly output;
    output.setX(a.getX() - b.getX());
    output.setY(a.getY() - b.getY());

    return output;
}

Osztaly Osztaly::operator+= (const Osztaly& a) {
    this->setX(this->getX() + a.getX());
    this->setY(this->getY() + a.getY());

    return *this;
}

Osztaly Osztaly::operator++ (int) {
    this->setX(this->getX() + 1);
    this->setY(this->getY() + 1);

    return *this;
}

/*
Osztaly& Osztaly::operator++ () {
    this->setX(this->getX() + 1);
    this->setY(this->getY() + 1);

    return *this;
}
*/

int main() {
    Osztaly o1, o2, o3;
    o1.setX(3); o1.setY(4); o2.setX(5); o2.setY(6);
    o3 = o1 + o2;
    cout << o3.getX() << " " << o3.getY() << endl;
    o3 += o1;
    cout << o3.getX() << " " << o3.getY() << endl;
    o3 += o1 + o2;
    cout << o3.getX() << " " << o3.getY() << endl;
    o3++;
    cout << o3.getX() << " " << o3.getY() << endl;

    return 0;
}
