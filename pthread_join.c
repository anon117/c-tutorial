#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define THREAD_NUMS 4

struct pthreadAdatok {
    int bemenet[2];
    int kimenet;
};

// pthread rutinok
void * add (void * strctPtr) {
    struct pthreadAdatok *pA = (struct pthreadAdatok *) strctPtr;
    int result;

    result = pA->bemenet[0] + pA->bemenet[1];
    pA->kimenet = result;
    pthread_exit(0);
}

void * sub (void * strctPtr) {
    struct pthreadAdatok *pA = (struct pthreadAdatok *) strctPtr;
    int result;

    result = pA->bemenet[0] - pA->bemenet[1];
    pA->kimenet = result;
    pthread_exit(0);
}

int main() {
    struct pthreadAdatok pA_Array[THREAD_NUMS];

    // inicializálás
    pA_Array[0].bemenet[0] = 16; pA_Array[0].bemenet[1] = 8;
    pA_Array[1].bemenet[0] = 12; pA_Array[1].bemenet[1] = 7;
    pA_Array[2].bemenet[0] = 14; pA_Array[2].bemenet[1] = 20;
    pA_Array[3].bemenet[0] = 11; pA_Array[3].bemenet[1] = 19;

    // szál azonosítók
    pthread_t threadIDs[THREAD_NUMS];

    // szálak létrehozása
    for (int i = 0; i < THREAD_NUMS; i++) {
        pthread_create(&threadIDs[i], NULL, add, &pA_Array[i]);
    }

    // szálak várakozása
    for (int i = 0; i < THREAD_NUMS; i++) {
        pthread_join(threadIDs[i], NULL);
        printf("%d. szál: %d + %d = %d\n", i, pA_Array[i].bemenet[0], pA_Array[i].bemenet[1], pA_Array[i].kimenet);
    }

   return 0;
}

