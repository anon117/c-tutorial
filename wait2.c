#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

pthread_t t1, t2;

int feltetel = 5;
int i = 0;

void * thread1(void *_p) {
    pthread_mutex_lock(&mutex);
    printf("T1: i = %d\n", i);
    while (feltetel >= i) {
        pthread_cond_wait(&cond, &mutex);
        printf("T1: i = %d\n", i);
    }
    printf("T1: Feltétel elfogadva!\n");
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}

void * thread2(void *_p) {
    for (int u = 0; u<= 10; u++) {
        pthread_mutex_lock(&mutex);
        i++;
        printf("T2: i = %d\n", i);
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
        usleep(1000000);
    }
    pthread_exit(0);
}

int main() {
    pthread_create( &t1, NULL, thread1, NULL);
    pthread_create( &t2, NULL, thread2, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    return 0;
}

