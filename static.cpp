#include <iostream>
#include <cstdlib>

using namespace std;

class Osztaly {
    public:
        static int count;
        Osztaly();
        static int getCount();
        ~Osztaly();
};

int Osztaly::count = 0;

Osztaly::Osztaly() {
    count++;
}

int Osztaly::getCount() {
    return count;
}

Osztaly::~Osztaly(){
    count--;
}

int main(){

    Osztaly o1, o2, o3;
    Osztaly * o4 = NULL;
    o4 = new Osztaly;
    Osztaly * o5 = new Osztaly;

    cout << o5->getCount() << endl;
    cout << Osztaly::getCount() << endl;
    delete(o4);
    delete(o5);
    // delete(o2); // a delete pointert vár...
    cout << Osztaly::getCount() << endl;
    return 0;
}
