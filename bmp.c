/*
gcc -o bmp bmp.c $(pkg-config --cflags --libs sdl2)

The code not working properly at this moment
*/
#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

struct Fejlec {
    unsigned char szignatura[2];
    unsigned char fajlmeret[4];
    unsigned char szabadterulet[4];
    unsigned char bitterkep_kezdocime[4];
};

struct InformaciosFejlec {
    unsigned char info_fejlec_meret[4];
    unsigned char szelesseg[4];
    unsigned char magassag[4];
    unsigned char megjelenites[2];
    unsigned char szinmelyseg[2];
    unsigned char tomorites[4];
    unsigned char bitterkep_merete[4];
    unsigned char vizszintes_felbontas[4];
    unsigned char fuggoleges_felbontas[4];
    unsigned char paletta_szinek[4];
    unsigned char hasznalt_szinek[4];
};

long decode(unsigned char * bajtok, size_t size) {
    long f = 0;

    int meret = size;
    f += (int)bajtok[meret-1];
    for (int i = meret-2; i >= 0; i--) {
        f <<= 8;
        f += (int)bajtok[i];
    }
    return f;
}

int main(){
    FILE * bmpfile;
    struct Fejlec fejlec;
    struct InformaciosFejlec infofejlec;
    bmpfile = fopen("640.bmp", "r");

    if (bmpfile == NULL)  {
        printf("Nem tudtam megnyitni!\n");
        exit(1);
    } else {
    }
    fread(&fejlec,     sizeof(struct Fejlec),            1, bmpfile);
    fread(&infofejlec, sizeof(struct InformaciosFejlec), 1, bmpfile);

    long SZELESSEG = decode(infofejlec.szelesseg, sizeof(infofejlec.szelesseg));
    long MAGASSAG = decode(infofejlec.magassag, sizeof(infofejlec.magassag));

    long FAJLMERET = decode(fejlec.fajlmeret, sizeof(fejlec.fajlmeret));
    long BITTERKEP_KEZDOCIME = decode(fejlec.bitterkep_kezdocime, sizeof(fejlec.bitterkep_kezdocime));
	long SZINMELYSEG =  decode(infofejlec.szinmelyseg, sizeof(infofejlec.szinmelyseg));
    long TOMORITES =  decode(infofejlec.tomorites, sizeof(infofejlec.tomorites));
    long BITTERKEP_MERET = FAJLMERET - BITTERKEP_KEZDOCIME;

    unsigned char *  bitterkep = (unsigned char *) malloc((unsigned char)BITTERKEP_MERET);
    size_t st = fread(bitterkep, 1, sizeof(bitterkep), bmpfile);
    printf ("Szinmélység: %d, Tömörítés: %d\n", SZINMELYSEG, TOMORITES);


/////////////// SDL ///
   SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    int i, j;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(SZELESSEG, MAGASSAG, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    
    int meret = 3;
    for (i = 0; i < SZELESSEG; ++i)
        for (j = 0; j < MAGASSAG; ++j) {         
            int B = (unsigned char) bitterkep[0+meret];
            int G = (unsigned char) bitterkep[1+meret];
            int R = (unsigned char) bitterkep[2+meret];

            SDL_SetRenderDrawColor(renderer, R, G, B, 0);
            SDL_RenderDrawPoint(renderer, i, j);     
            meret += 3;       
        }
    SDL_RenderPresent(renderer);
    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
