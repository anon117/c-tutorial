/*
 Fordítás:
   gcc -lstdc++ -o 01-class2 01-class.cpp
   g++ -o 01-class 01-class.cpp
 */

#include <iostream>

using namespace std;

class Teglalap {
  int szelesseg, hosszusag;

  public:
  // Prototípus
  void setErtekek (int, int);
  int terulet() {
    return szelesseg * hosszusag;
  }
};

void Teglalap::setErtekek(int x, int y) {
    szelesseg = x;
    hosszusag = y;
}

// --------------------------------
/*
   Overloading constuctors
 */

class Negyzet {
  int hosszusag;
  public:
    // Konstruktor prototípus
    Negyzet();
    Negyzet (int);

    int terulet(void) {
      return hosszusag * hosszusag;
    }
};

Negyzet::Negyzet () {
  hosszusag = 6;
}

Negyzet::Negyzet (int hossz) {
  hosszusag = hossz;
}

// --- Pointer to class
class Circle {
  double radius;
  const double PI = 3.1415926535;

  public:
  Circle(double r);
  double area();
};


Circle::Circle(double r) {
  radius = r;
}

double Circle::area() {
  return radius * radius * PI;
}

// --- main ---

int main() {
    Teglalap teglalap1;
    teglalap1.setErtekek(4, 5);

    Negyzet negyzet1(5);
    Negyzet negyzet2; // <--- nem jó a `negyzet2()´

    // Ez gyakorlatilag egy iniciálatalan Circle objektum (null)
    Circle * circle1;
    // most inicializáljuk
    circle1 = new Circle(4.2);

    std::cout << "Téglalap terület: " << teglalap1.terulet() << endl;
    std::cout << "Négyzet területe: " << negyzet1.terulet() << endl;
    std::cout << "Négyzet területe: " << negyzet2.terulet() << endl;
    std::cout << "Kör területe: " << circle1->area() << endl;
    return 0;
}



