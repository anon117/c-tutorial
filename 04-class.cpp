/*
 Itt az operátor túlterheléssel fogunk foglalkozni
 (operator overloading)

 Két vektort fogunk összeadni, a két vektor egy-egy osztály lesz két-két x,y
 vektorértékkel. v1(1,2), v2(3,4).
 */

#include <iostream>

using namespace std;

// először hozzuk létre a vektorosztályt
class Vector {
  public:
    // a két member
    int x,y;
    // konstruktor
    Vector() {} // Fordító nyafog ha ez nincs.
    Vector(int a, int b) : x(a), y(b) {}
};

// Most következik az `összeadás´ ``+´´ operátor túlterhelése, vagy
// átdeginiáljuk a működését, mivel alapban csak primitív adattípusokat tud
// összeadni. Ez a felüldefiniálás csak erre az osztályre érvényes.

// Paraméterben meg kell adnunk a két összeadandó vektort referenciáját, hogy
// tudja mire számítson a paraméterekben, vagyis az `+´ bal és jobb oldalán
// milyen operandusok lehetnek.
Vector operator+ (const Vector& bal, const Vector& jobb) {
  Vector kimenet; // Ez lesz a művelet után a visszadatott eredmény.
  // A megfelelő vektorértékeket összeadjuk.
  kimenet.x = bal.x + jobb.x;
  kimenet.y = bal.y + jobb.y;
  return kimenet;
}

int main() {
  Vector elso(3,4); // Az egyik vektor.
  Vector masodik(5,6); // A másik...
  Vector result; // Az eredmény.

  result = elso + masodik;
  cout << "A vector összege: (" << result.x << "," << result.y << ")" << endl;
}
