#include <cstdlib>
#include <iostream>

using namespace std;

class Osztaly {
  private:
    int result;
  public:
    Osztaly(){}
    Osztaly& plusz(int i, int j);
    Osztaly& minusz(int i, int j);
    int getResult();
};

Osztaly& Osztaly::plusz(int i, int j) {
  result = i + j;
  return *this;
}

Osztaly& Osztaly::minusz(int i, int j) {
  result = i - j;
  return *this;
}

int Osztaly::getResult() {return result;}

int  main(){
  Osztaly o;
  int res =  o.plusz(3, 4).minusz(2,5).getResult(); 
  cout << res << endl;
  return 0;
}
