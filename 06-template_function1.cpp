#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

template <typename TYPE>
TYPE pluszegy(TYPE bemenet) {
  return bemenet + 1;
}

template <typename TYPE>
TYPE add(TYPE elso, TYPE masodik) {
  return elso + masodik;
}

int main() {
  int i = pluszegy<int>(2);
  double d = pluszegy<double>(2.11);
  cout << i << endl;
  cout << d << endl;

  int ii = add<int>(4,5);
  cout << ii << endl;

  return 0;
}
