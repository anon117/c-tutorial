#include <iostream>
#include <string>
#include <cstdlib>

#include "09-teglalap.h"

using namespace std;

int main() {
  Teglalap teglalap1(4, 6);
  int i = teglalap1.terulet();

  cout << "Eredmény: " << i << endl;

  return 0;
}
