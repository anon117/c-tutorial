#include <iostream>
#include <cstdlib>

using namespace std;

int plusz(int i, int j);
int minusz(int i, int j);
int szorzas(int i, int j);

int operands[10] = {1,2,3,4,5,6,7,8,9,10};
int sizeofop = 10;

int array_feldolgozo(int arr[], int size, int (*fn)(int, int)) {
  int result = 0;;
  for(int i = size-1 ; i>0 ; i--) {
    result = fn(arr[i-1], result);
  }

  return result;
}

int main() {

  cout << array_feldolgozo(operands, sizeofop, plusz) << endl;
  cout << array_feldolgozo(operands, sizeofop, minusz) << endl;
  cout << array_feldolgozo(operands, sizeofop, szorzas) << endl;
  return 0;
}

int plusz(int i, int j) {return i + j;}
int minusz(int i, int j) {return i - j;}
int szorzas(int i, int j) {return i * j;}

