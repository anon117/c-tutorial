#include <cstdlib>
#include <iostream>

using namespace std;

// prototípusok
int add1(int i);
int add2(int i);
int add3(int i);

// array of fn
int (*fn_ptr[])(int) = {add1, add2, add3};
int result;
int params[3] = {8, 4, 5};

int main() {
  result =  (*fn_ptr[0])(8);
  result += (*fn_ptr[1])(4);
  result += (*fn_ptr[2])(5);

  cout << result << endl;

  result = 0;

  for (int i = 0; i <= 2; i++) {
    result += (*fn_ptr[i])(params[i]);
  }

  cout << result << endl;
  return 0;
}

// Deklarációk
int add1(int i) {return i+1;}
int add2(int i) {return i+2;}
int add3(int i) {return i+3;}

