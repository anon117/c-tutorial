/*
fstp[s|t|d]: single, tbyte, double
*/

#include <iostream>
#include <cstdlib>
#include <bitset>

using namespace std;

int main() {
  bitset<80> FPU_OUTPUT;

  double a = 0.1; 
  double b = -1.1;


  __asm__ (
      "finit;"
      "fldpi;"
      "fstpt %0" : "=m"(FPU_OUTPUT) 
      );

      cout << FPU_OUTPUT << endl;

   __asm__ (
      "finit;" 
      "fld %1;"
      "fld %2;"
      "faddp;"
      "fstpl %0" : "=m"(FPU_OUTPUT) : "m"(a),  "m"(b)
      ); // 2370
     
      cout << FPU_OUTPUT << endl;
}
