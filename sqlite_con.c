#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>

/*
 Megjegyzések...

 Fordítás: gcc -o sqlite_test sqlite_test.c -lsqlite3

 Header:
 Arch/Manjaro alatt felrakja az `sqlite3´ csomag a header fájl is
 Debian/Ubuntu alatt szükséges a `sqlite3 libsqlite3-0 libsqlite3-dev´ csomagok.

 */

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int main() {
  char *hibaUzenet, *sql;
  sqlite3  *adatbazis;
  int rc;

  const char *data = "Callback lefutott";

  rc = sqlite3_open("test.db", &adatbazis);

  if (rc) {
    fprintf(stderr, "Nem tudtam megnyitni az adatbázist: %d.\n", sqlite3_errmsg(adatbazis));
    return 0;
  } else {
    fprintf(stderr, "Az adatbázist sikeresen megnyitotam.\n");
  }

   /* Create SQL statement */
   sql = "CREATE TABLE COMPANY("  \
         "ID INT PRIMARY KEY     NOT NULL," \
         "NAME           TEXT    NOT NULL," \
         "AGE            INT     NOT NULL," \
         "ADDRESS        CHAR(50)," \
         "SALARY         REAL );";

  rc = sqlite3_exec(adatbazis, sql, callback, 0, &hibaUzenet);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "Nem tudtam létrehozni az adattáblát: %d.\n", sqlite3_errmsg(adatbazis));
  } else {
    fprintf(stdout, "A tábla sikeresen létrejött.\n");
  }

  // Készítek egy triggert, ami akkor aktiválódik, ha egy sort szúrok be a
  // fenti táblába. erről fog egy log sort létrehozni.

  // létrehozom a log táblát
  sql = "CREATE TABLE COMPANY_LOG (" \
         "ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " \
         "DATUM TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, " \
         "LOG_MESSAGE VARCHAR(200)" \
         ");";

  rc = sqlite3_exec(adatbazis, sql, callback, 0, &hibaUzenet);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "Nem tudtam létrehozni az adattáblát: %s.\n", sqlite3_errmsg(adatbazis));
  } else {
    fprintf(stdout, "A tábla sikeresen létrejött.\n");
  }

  sql = "CREATE TRIGGER COMPANY_LOG AFTER INSERT ON COMPANY " \
         "BEGIN " \
         "INSERT INTO COMPANY_LOG (LOG_MESSAGE) VALUES ('Beszúrás történt'); " \
         "END;";

  
  rc = sqlite3_exec(adatbazis, sql, callback, 0, &hibaUzenet);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "TRIGGER létrehozási hiba: : %s.\n", sqlite3_errmsg(adatbazis));
  } else {
    fprintf(stdout, "A TRIGGER sikeresen létrejött.\n");
  }

     /* Create SQL statement */
   sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (1, 'Paul', 32, 'California', 20000.00 ); " \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "     \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
         "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );" \
         "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
         "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";

   rc = sqlite3_exec(adatbazis, sql, callback, 0, &hibaUzenet);

   if( rc != SQLITE_OK ){
      fprintf(stderr, "SQL error: %s\n", hibaUzenet);
      sqlite3_free(hibaUzenet);
   } else {
      fprintf(stdout, "Records created successfully\n");
   }

   sql = "SELECT * from COMPANY";

   rc = sqlite3_exec(adatbazis, sql, callback, (void*)data, &hibaUzenet);

   if( rc != SQLITE_OK ) {
      fprintf(stderr, "SQL error: %s\n", hibaUzenet);
      sqlite3_free(hibaUzenet);
   } else {
      fprintf(stdout, "Operation done successfully\n\n");
   }
  
  // --- sqlite3_prepare_v2() --- 
  sqlite3_stmt *stmt; 
  sql = "SELECT * from COMPANY";
  int result = sqlite3_prepare_v2(adatbazis, sql, -1, &stmt, NULL);
  if (rc != SQLITE_OK) {
    printf("error: ", sqlite3_errmsg(adatbazis));
    return 0;
  }

  while ((result = sqlite3_step(stmt ) == SQLITE_ROW)) {
    int id = sqlite3_column_int(stmt, 0);
    const char *name = sqlite3_column_text(stmt, 1);
    int age = sqlite3_column_int(stmt, 2);
    const char *address = sqlite3_column_text(stmt, 3);
    int salary = sqlite3_column_int(stmt, 4);

    printf("Id: %d, Name: %s, Age: %d, Address: %s, Salary: %d.\n", id, name, age, address, salary);
  }

  if (rc != SQLITE_DONE) {
    printf("error: ", sqlite3_errmsg(adatbazis));
  }

  sqlite3_finalize(stmt);

  sqlite3_close(adatbazis);

  return 0;
}
