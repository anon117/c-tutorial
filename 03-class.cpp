/*
 Itt a this kulcsszóval fogunk foglalkozni, ami az osztály önmaga referenciáját
 adja vissza.
 */

#include <iostream>

using namespace std;

class Osztaly {
  public:
  bool kivagyok(Osztaly& bemenet);
};

bool Osztaly::kivagyok(Osztaly& bemenet) {
  if (&bemenet == this) {
    return true;
  } else {
    return false;
  }
}

int main() {
  Osztaly alfa;
  // A beta Osztaly mutatóját inicializálom az alfa objektum referenciájával.
  Osztaly * beta = &alfa;

  if (beta->kivagyok(alfa)) {
    cout << "&alfa az beta." << endl;
  }
}
