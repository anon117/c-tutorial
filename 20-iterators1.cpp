#include <iostream>
#include <cstdlib>

#include <vector>
#include <iterator>

using namespace std;

vector<int> vector1 = {2,6,4,8,1,3,10,23,17,0,9};
vector<int> vector2 = {0,0,0};

vector<int>::iterator ptrVector1;
vector<int>::iterator ptrBegin = vector1.begin();
vector<int>::iterator ptrEnd = vector1.end();

int main() {
    for (ptrVector1 = vector1.begin(); ptrVector1 < vector1.end(); ptrVector1++) {
        cout << *ptrVector1 << " ";
    }
    cout << endl;

    for (ptrVector1 = vector1.end()-1; ptrVector1 >= vector1.begin(); ptrVector1--) {
        cout << *ptrVector1 << " ";
    }
    cout << endl;

    ptrVector1 = vector1.begin();
    while (ptrVector1 < vector1.end()) {
        cout << *ptrVector1 << " ";
        ptrVector1++;
    }
    cout << endl;

    ptrVector1 = vector1.end()-1;
    while (ptrVector1 >= vector1.begin()) {
        cout << *ptrVector1 << " ";
        ptrVector1--;
    }

    cout << endl;
////////////////////////////////////////
//
//
    auto newBegin = next(ptrBegin,2);
    auto newEnd = prev(ptrEnd, 3);

    cout << *newBegin << endl;
    cout << *newEnd << endl;

    for (ptrVector1 = newBegin; ptrVector1 < newEnd; ptrVector1++) {
        cout << *ptrVector1 << " ";
    }
    cout << endl;
///////////////////////////////////////
//
    advance(ptrBegin, 5); //5. pozicióra áll
    copy(vector2.begin(), vector2.end(), inserter(vector1, ptrBegin));

    for (int &x : vector1)
        cout << x << " ";
    cout << endl;

    for (ptrVector1 = vector1.begin(); ptrVector1 < vector1.end(); ptrVector1++) {
        cout << *ptrVector1 << " ";
    }
    cout << endl;

   return 0;
}
