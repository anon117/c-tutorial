#include <iostream>

using namespace std;

class Circle {
  double radius;
  const double PI = 3.1415926535;

  public:
  Circle(double r) : radius(r) {}
  double area();
};

double Circle::area() {
  return radius * radius * PI;
}

class Henger {
  Circle base; // Alapul veszi a Circel osztályt.
  double magassag;

  public:
  Henger(double r, double m) : base(r), magassag(m) {}
  double terfogat() {
    // az alap (Circle) osztály függvényét használja
    return base.area() * magassag;
  }
};

int main() {
  Circle circle1(4.2);
  Henger henger1(6, 8);
  cout << "Kör területe: " << circle1.area() << endl;
  cout << "Henger térfogata: " << henger1.terfogat() << endl;
}
