#include <iostream>
#include <cstdlib>

using namespace std;

class Alap {
  private:
    int x, y;
  public:
    int b1, b2;
    int fn1(void) {return x;}
    int fn2(void) {return y;}
};

class Szarmaztatott: private Alap {
  int b2; // az örökölt b2 elfedése
  int d;
  public:
    using Alap::b1; // A private származtatású b1 public elérésü lesz
    void fn1(void);  // Az örökölt fn1 elfedése
};

void Szarmaztatott::fn1(void) {
  d = Alap::fn1(); // a nem látható fn1 elérése
  b1 = Alap::fn2();
  b2 = Alap::b2;
}

int main() {
  Szarmaztatott od;
  od.b1 = 23;

  return 0;
}
