#include <iostream>
#include <cstdlib>
#include <math.h>

#define EarthRadius 6372.8 // Km

double FokotRadianba(double fok) {
    return M_PI * fok / 180;
}

double HaversineTavolsag(double p1_lat, double p1_long, double p2_lat, double p2_long) {
    double p1_lat_Rad = FokotRadianba(p1_lat);
    double p2_lat_Rad = FokotRadianba(p2_lat);
    double p1_long_Rad = FokotRadianba(p1_long);
    double p2_long_Rad = FokotRadianba(p2_long);

    double diffLat = p2_lat_Rad - p1_lat_Rad;
    double diffLong = p2_long_Rad - p1_long_Rad;

    double compute = asin(sqrt(sin(diffLat / 2) * sin(diffLat / 2) + cos(p1_lat_Rad) * cos(p2_lat_Rad) * sin(diffLong / 2) * sin(diffLong / 2)));
    return 2 * EarthRadius * compute;
}

double HaversineTavolsag2(double p1_lat, double p1_long, double p2_lat, double p2_long) {
    double diffLat = FokotRadianba(p2_lat - p1_lat);
    double diffLong = FokotRadianba(p2_long - p1_long);

    p1_lat = FokotRadianba(p1_lat);
    p2_lat = FokotRadianba(p2_lat);

    double a = sin(diffLat / 2) * sin(diffLat / 2) + sin(diffLong / 2) * sin(diffLong / 2) * cos(p1_lat) * cos(p2_lat);
    return EarthRadius * 2 * asin(sqrt(a));
}

int main() {
    std::cout << "Distance = " << HaversineTavolsag(36.12, -86.67, 33.94, -118.4) << " Km" << std::endl;
    std::cout << "Distance = " << HaversineTavolsag2(36.12, -86.67, 33.94, -118.4) << " Km" << std::endl;
    return 0;
}
