#include <iostream>
#include <cstdlib>

using namespace std;

struct Node {
  int Elem;
  Node * next;
};

class LList {
  Node *HEAD;
  Node *Aktualis;
  public:
    LList();
    void addElem(int i);
    void traversLList();
};

// Inicializálom a listát, a HEAD lesz a feje
LList::LList() {
  HEAD = NULL;
}

void LList::addElem(int i) {
  Node * uj = new Node();
  uj->Elem = i;
  uj->next = NULL;

  if (HEAD == NULL) {
    cout << "Elso elem" << endl;
    HEAD = uj;
    Aktualis = uj;
  } else {
    cout << "Többi elem" << endl;
    Aktualis->next = uj;
    // Nem az étéket adja át, hanem az "uj" referenciát.
    // Ezért kell a ->next-ben eltárolni az "uj" referenciat,
    // mert felülíródik.
    Aktualis = uj;
  }
}

void LList::traversLList(){
  cout << "Listázás..." << endl;
  Aktualis = HEAD;
  while(Aktualis != NULL) {
    cout << Aktualis->Elem << endl;
    Aktualis = Aktualis->next;  
  }
}

int main() {
  LList ll;
  ll.addElem(8);
  ll.addElem(4);
  ll.addElem(6);
  ll.addElem(10);
  ll.addElem(14);
  ll.addElem(7);

  ll.traversLList();

  return 0;
}
