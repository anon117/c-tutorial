#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class Osszead {
  int x, y;

  public:
  Osszead(){}
  Osszead(int input_x, int input_y) : x(input_x), y(input_y) {}
  int add();
};

int Osszead::add() {
  return x + y;
}

template <class TYPE>
class TOsszead {
  TYPE x, y;
  public:
  TOsszead() {};
  TOsszead (TYPE input_x, TYPE input_y) : x(input_x), y(input_y) {};
  TYPE add();
};

/*
  Ez itt a template osztály metódusának a prototípusa. 
  Itt a második <TYPE> segítsgével mondod meg, hogy milyen adattípust várjon a
  template osztály a példányosítás során.
*/
template <class TYPE>
TYPE TOsszead<TYPE>::add() {
  return x + y;
}

int main() {
  Osszead  osszead1(8, 9);
  TOsszead<int> osszead2(8, 9);
  TOsszead<double> osszead3(8.3, 9.1);
  TOsszead<string> osszead4("kutya", "cica");
  cout << osszead1.add() << endl;
  cout << osszead2.add() << endl;
  cout << osszead3.add() << endl;
  cout << osszead4.add() << endl;

  return 0;
}

