#include <iostream>
#include <cstdlib>

using namespace std;

class Singleton {
    private:
        static Singleton * instance;
        Singleton();
    public:
        static Singleton * getInstance();
};

Singleton* Singleton::instance = 0;
Singleton::Singleton(){}
Singleton * Singleton::getInstance() {
    if (instance == 0) {
        instance = new Singleton();
    }
    return instance;
}

int main(){
    // new Singleton();  // Privát konstruktort hivna, de nem megy.
    Singleton * s = NULL; s = Singleton::getInstance();
    Singleton * r = Singleton::getInstance();

    cout << s << " " << r << endl;

    return 0;
}
