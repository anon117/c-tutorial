#include <iostream>
#include <cstdlib>

using namespace std;

struct Node {
  int Elem;
  Node * next;
  Node * prev;
};

class DLList {
  Node *HEAD;
  Node *Aktualis;
  public:
    DLList();
    void addElem(int i);
    void traversDLListBackward();
    void traversDLListForward();
};

DLList::DLList(){
  HEAD = NULL;
}

void DLList::addElem(int i) {
  Node * uj = new Node();
  uj->Elem = i;
  uj->next = NULL;
  uj->prev = NULL;

  if (HEAD == NULL) {
    // cout << "Elso elem" << endl;
    HEAD = uj;
    Aktualis = uj;

    // cout << Aktualis->Elem << " " << Aktualis->next << " " << Aktualis->prev << endl;

  } else {
    // cout << "Többi elem" << endl;
    Aktualis->next = uj;
    uj->prev = Aktualis; // nem Aktualis->prev = Aktualis;
		
    Aktualis = uj;    
	// cout << Aktualis->Elem << " " << Aktualis->next << " " << Aktualis->prev << endl;
  }
}

void DLList::traversDLListForward(){
  cout << "Listázás előre..." << endl;
  Aktualis = HEAD;
  while(Aktualis != NULL) {
    cout << Aktualis->Elem << " ";
	// cout << Aktualis->Elem << " " << Aktualis->next << " " << Aktualis->prev << endl;
    Aktualis = Aktualis->next;  
   }
   cout << endl;
}

void DLList::traversDLListBackward(){
  cout << "Listázás visszafelé..." << endl;

  // Lista végére megy
  Aktualis = HEAD;
  while(Aktualis->next != NULL) {Aktualis = Aktualis->next;}
  
  while(Aktualis != NULL) {
	cout << Aktualis->Elem << " ";
	// cout << Aktualis->Elem << " " << Aktualis->next << " " << Aktualis->prev << endl;
    Aktualis = Aktualis->prev;  
  }
  cout << endl;
}

int main() {
  DLList ll;
  ll.addElem(8);
  ll.addElem(4);
  ll.addElem(6);
  ll.addElem(10);
  ll.addElem(14);
  ll.addElem(7);
  
   
  ll.traversDLListForward();
  ll.traversDLListBackward();
  
  return 0;
}


