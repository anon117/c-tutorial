#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

#define osszead(a, b) a + b

int main() {
  #ifndef ARRAY_SIZE
  #define ARRAY_SIZE 100
  #endif
  int table[ARRAY_SIZE];

  #ifdef ALMAFA
  cout << "DEFINIÁLVA" << endl;
  #else
  cout << "NINCS DEFINIÁLVA" << endl;
  #endif

  cout << __DATE__ << " - " << __TIME__ << endl;
  cout << __FILE__ << " - " << __LINE__ << endl;
  cout << "C++ standard: " << __cplusplus << endl;
////  cout << "C standard: " << __STDC_VERSION__ << endl;

  cout << osszead(3, 4) << endl;
  cout << osszead(3.1, 4.2) << endl;
  cout << osszead(3, 4.5) << endl;
////  cout << osszead("alma","fa") << endl;
  return 0;
}
