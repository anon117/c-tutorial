#include <stdio.h>

struct S {
  unsigned int bits : 3; // 3 bit unsigned field
};

int main() {
  struct S s = {0};

  for (int i = 0; i <= 20; i++) {
    s.bits = i;
    printf("%d\n", s.bits);
  }

  return 0;
}
