#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

// ------

double szamolas (double (*fn_ptr)(double, double), double be1, double be2) {
  return fn_ptr(be1, be2);
}

double plusz(double a, double b) {
  return a + b;
}

// ------

template <typename TYPE>
TYPE Tszamolas (TYPE (*fn_ptr)(TYPE, TYPE), TYPE be1, TYPE be2) {
  return fn_ptr(be1, be2);
}

template <typename TYPE>
TYPE Tplusz(TYPE a, TYPE b) {
  return a + b;
}

// -------

int main() {
  string str1 = "rántott"; string str2 = "hús";
  cout << szamolas(plusz, 1, 3) << endl;
  cout << Tszamolas(Tplusz, 1, 3) << endl;
  cout << Tszamolas(Tplusz, 1.4, 3.8) << endl;
  cout << Tszamolas(Tplusz, str1, str2) << endl;

  return 0;
}
